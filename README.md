# README # 

## Author: Xing Qian, xingq@uoregon.edu ##

This is a solution of the assignment "project3" project for CIS 322

You can view the project description: https://bitbucket.org/UOCIS322/proj3-jsa/src/master/

## Introduction of my solution

The assignment asked for rebuild the flask vocab game by jQuery Ajax, and I have reached this goal.

## Run the project

```
# go to the vocab folder, and run the following command.
# at first, we need to build the image and run the image
sh ./run.sh

# tests the page by nosetests
nosetests
```

## Details for this assignment

- I use jQuery to send Ajax requests when click the submit button, and display the results in the `vocab.html`.
- I modified the method `check` in `flask_vocab.py`, and change its response to json.
- I delete the `flask_minijax.py` and `templates/minijax.html`.
- I delete the route `keep going` because we don't need it anymore.
- It would jump to success page if entered 3 matched words.
- (Extra) Highlight matched parts when inputting.
- (Extra) Grey-out matched words.
- (Extra) Remove letters which could not found in anagram.